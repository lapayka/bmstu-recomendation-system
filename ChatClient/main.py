from natasha import Segmenter, NewsEmbedding, NewsSyntaxParser, Doc
from pymorphy2 import MorphAnalyzer
import requests
import json

debug = False

emb = NewsEmbedding()
segmenter = Segmenter()
syntax_parser = NewsSyntaxParser(emb)
analyzer = MorphAnalyzer()

def PrettyOutput(_json):
    res = ""
    __json = json.loads(_json)
    for item in __json:
        res = res + "\n" + f"{item['name']:60}{item['country']:10}{item['shape']:10}{item['price']:10}{item['fretCount']:10}"  

    return res

def get_document(text :str):
    doc = Doc(text)
    doc.segment(segmenter)
    doc.parse_syntax(syntax_parser)

    for i in range(len(doc.tokens)):
        an = analyzer.parse(doc.tokens[i].text)
        doc.tokens[i].text = an[0].normalized.word
        

    if debug:
        doc.sents[0].syntax.print()
        print(doc.tokens)
    
    return doc

guitar_types = set(["акустический", "классический", "электрический", "концертный", "дредноут"])

class YaNePonyalCommand:
    def execute(self):
        return "Я сломался уточните запрос"

class CommandTellAboutAll:
    def execute(self):
        return "Гитара -- струнный инструмент с 6-ю струнами. Гитары бывают электрические и акустические. Акустические в свою очередь подразделяются на классические и акустические."

def countryToEng(country : str):
    if country == "США":
        return "USA"
    elif country == "Япония":
        return "Japan"
    elif country == "Мексика":
        return "Mexico"

class FiterCommand:
    def __init__(self):
        self.price_filter = 100000
        self.fret_filter = 0
        self.country_filter = None

    def execute(self):
        tail = ""
        if self.country_filter != None:
            tail = f" из страны {self.country_filter}"

        data = {"price": self.price_filter, "fret": self.fret_filter}
        if self.country_filter != None:
            data["country"] = countryToEng(self.country_filter)

        response = requests.post("http://localhost:8000/api/v1/guitars/filtered", json=data)

        return PrettyOutput(response.content)
    
    def SetPriceFilter(self, upper_bound):
        self.price_filter = upper_bound
    def SetFretFilter(self, lower_bound):
        self.fret_filter = lower_bound
    def SetCountryFilter(self, country):
        self.country_filter = country

def dispatch_country(token):
    ret = None
    if token in ["японский", "япония"]:
        ret = "Япония"
    elif token in ["сша", "американский", "пендосский"]:
        ret = "США"
    elif token in ["мексиканский", "мексика"]:
        ret = "Мексика"
    
    if debug:
        print(f'dispatch of {token} is {ret}')
    return ret

class CommandTellAboutClass:
    def __init__(self, _type) -> None:
        self.type = _type
        self.bad = False
        if _type not in guitar_types:
            self.bad = True

    def execute(self):
        if self.type == "акустический":
            return "Акустические гитары имеют стальные струны. Акустические гитары бывают:\nКонцертные\nДредноуты"
        if self.type == "классический":
            return "Классические гитары имеют нейлоновые струны..."
        if self.type == "электрический":
            return "Электрические гитары имеют нейлоновые струны и бывают разных форм:\nStratocaster\nTelecaster\nLes Paul\nES45\n..."
        if self.type == "концертный":
            return "Мощные акустические гитары с мощным звуком, которые не могут не порадовать соседей"
        if self.type == "дредноут":
            return "Мощные акустические гитары"
        return f"Не знаю таких гитар, могу рассказать про {guitar_types}"

def find_tocken_with_id(id :str, tokens):
    for token in tokens:
        if id == token.id:
            return token
        
    return None

def dispatch_tell_command(doc: Doc, guitar_node):
    describe_tokens = []

    for token in doc.tokens:
        if token.head_id == guitar_node.id:
            describe_tokens.append(token)

    command = CommandTellAboutAll()

    if len(describe_tokens) != 0:
        for dtoken in describe_tokens:
            if dtoken.rel == "nmod" or dtoken.rel == "amod":
                command = CommandTellAboutClass(dtoken.text)
                if not command.bad:
                    return command
        
    return command
        
def dispatch_filter_command(guitar_node, doc : Doc):
    command = FiterCommand()

    for token in doc.tokens:
        if token.rel == "nummod":
            try:
                num = int(token.text)
            except ValueError:
                continue

            parent = find_tocken_with_id(token.head_id, doc.tokens)
            if parent != None and parent.text == 'тысяча':
                num *= 1000

            if 19 <= num <= 24:
                command.SetFretFilter(num)
            else:
                command.SetPriceFilter(num)

    describe_tokens = []
    for token in doc.tokens:
        if token.head_id == guitar_node.id:
            describe_tokens.append(token)

    if len(describe_tokens) != 0:
        for dtoken in describe_tokens:
            if dtoken.rel == "nmod" or dtoken.rel == "amod":
                country = dispatch_country(dtoken.text)
                if country != None:
                    command.SetCountryFilter(country)

    return command

        

def dispatchCommand(doc : Doc):
    tokens = doc.tokens

    guitar_node = None
    for token in doc.tokens:
        if token.text == 'гитара':
            guitar_node = token
            break
        if token.text == 'дредноут':
            guitar_node = token
            break

    head_token = find_tocken_with_id(guitar_node.head_id, tokens=tokens)

    if head_token.text in ['рассказать', "поведать", "бывать"]:
        return dispatch_tell_command(guitar_node=guitar_node, doc=doc)
    
    if head_token.text in ['посоветовать', 'порекомендовать']:
        return dispatch_filter_command(guitar_node=guitar_node, doc=doc)


    return YaNePonyalCommand()

while True:
    doc = get_document(input("Чего изволите:\n"))

    command = dispatchCommand(doc)

    print(f"{command.execute()}\n")
