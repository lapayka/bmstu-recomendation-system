#pragma once
#include "ICriterion.h"

class CountryCriterion :
    public ICriterion
{
public:
    CountryCriterion(const std::string &country, double weight) : m_weight(weight), m_country(country) {}

    virtual double GetDistance(const ICriterionPtr& rCriterion) const override;
    virtual double GetWeight() const override { return m_weight; }

    virtual ~CountryCriterion() = default;

protected:
    virtual std::string GetName() const {return m_name;}

private:
    std::string m_country;
    double m_weight = 1.;
    std::string m_name = "CountryCriterion";
};

using CountryCriterionPtr = std::shared_ptr<CountryCriterion>;