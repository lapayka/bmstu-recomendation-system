#pragma once

#include <vector>
#include <filesystem>

#include "GuitarItem.h"

class ITableBuilder
{
public:
	virtual std::vector<GuitarItem> BuildGuitarItems(const std::filesystem::path& file_path) const = 0;

	virtual ~ITableBuilder() = default;
};


using ITableBuilderPtr = std::shared_ptr<ITableBuilder>;
