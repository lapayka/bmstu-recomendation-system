#pragma once

#include <memory>
#include <vector>

#include "CriterionTable.h"

class IRecomendationStrategy {
public:
    virtual std::vector<int> GetRecomendationList(int itemId, const CriterionTablePtr& CTable) const = 0;
    virtual std::vector<int> GetRecomendationList(const std::vector<int>& likedIds, const std::vector<int>& disLikedIds, const CriterionTablePtr& CTable) = 0;
    virtual std::vector<double> GetUndorderedRateList(int itemId, const CriterionTablePtr& CTable) const = 0;

    virtual ~IRecomendationStrategy() = 0;
};

using IRecomendationStrategyPtr = std::shared_ptr<IRecomendationStrategy>;
