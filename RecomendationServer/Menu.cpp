#include "Menu.h"

#include <iostream>
#include <iomanip>

#include "CountryFilter.h"
#include "PirceFilter.h"
#include "FretFilter.h"
#include "TypeFilter.h"
#include "HasTunerFilter.h"

static std::vector<GuitarItem> FilterItems(const std::vector<GuitarItem>& items, const Filters& filters)
{
	std::vector<GuitarItem> res;

	for (const auto& item : items)
	{
		bool check = true;

		for (const auto& filter : filters)
		{
			if (!filter->Check(item))
			{
				check = false;
				break;
			}
		}

		if (check)
			res.push_back(item);
	}

	return res;
}

void Menu::start()
{
	while (1)
	{
		try {
			std::cout
				<< "1. Output DataSet\n"
				<< "2. Get Recomendations\n"
				<< "3. Filter\n"
				<< "4. Get Recomendations then filter\n"
				<< "5. Get Filter then get recomendations\n";

			std::cout << "\nYour choice: ";

			int choice;
			std::cin >> choice;

			switch (choice)
			{
			case 1:
			{
				OutputItems(m_items);
				break;
			}
			case 2:
			{
				auto recs = RecomendationSystemStart();
				OutputItemsByIdList(recs);
				break;
			}
			case 3:
			{
				auto recs = FilterSystemStart();
				OutputItems(recs);
				break;
			}
			case 4:
			{
				auto recs = RecomendationSystemStart();
				OutputItemsByIdList(recs);
				auto filters = FilterSystemStart();
				OutputItems(filters);
				continue;
			}
			case 5:
			{
				auto filters = FilterSystemStart();
				OutputItems(filters);
				auto recs = RecomendationSystemStart(filters);
				OutputItemsByIdList(recs);
				continue;
			}
			default:
				std::cout << "WTF!!!" << std::endl;
				continue;
			}
		}
		catch (const std::exception& e) {
			std::cout << e.what();
		}
	}
}

std::vector<int> Menu::RecomendationSystemStart() {
	return RecomendationSystemStart(m_items);
}

std::vector<int> Menu::RecomendationSystemStart(const std::vector<GuitarItem> &items)
{
	std::cout << "Choose liked items (input count): ";
	size_t size;
	std::cin >> size;

	std::vector<int> liked_res(size);
	for (size_t i = 0; i < size; i++)
	{
		std::cout << "Liked item id: ";
		std::cin >> liked_res[i];
	}

	std::cout << "Choose disliked items (input count): ";
	std::cin >> size;

	std::vector<int> disliked_res(size);
	for (size_t i = 0; i < size; i++)
	{
		std::cout << "Liked item id: ";
		std::cin >> disliked_res[i];
	}

	return m_recStrategy->GetRecomendationList(liked_res, disliked_res, m_criterionBuilder->BuildCriterionTable(items));
}

template<typename FltType, typename ...Args>
static IFilterPtr Filter(Args &&...args) {
	return std::make_shared<FltType>(std::forward<Args>(args)...);
}

static void PrintTypes() {	
	std::cout << "Choose tyoe:\n";

	int i = 0;
	for(const auto &s : shapeToStringMap)
		std::cout << ++i << "\t" << s.second << std::endl;
}

static IFilterPtr GetTypesFilter(int choise) {
	auto filter = filtersArray[choise - 1];
	
	return Filter<TypeFilter>(filter);
}

static void GetPriceBoundaries(bool& low, int& lowValue, bool& high, int& highValue) {
	std::string tmp;
	std::cout << "Choose low boundary price(y/n): ";
	std::cin >> tmp;
	if (tmp == "y") {		
		std::cout << "Low price boundary: ";
		low = true;
		std::cin >> lowValue;
	}

	std::cout << "Choose high boundary price(y/n): ";
	std::cin >> tmp;
	if (tmp == "y") {
		std::cout << "High price boundary: ";
		high = true;
		std::cin >> highValue;
	}

	std::cout << std::endl;
}

static void GetPopularPriceBoundaries(bool& low, int& lowValue, bool& high, int& highValue) {
	static std::unordered_map<std::string, char> prices = {
		{ "average", 'm' },
		{ "above-average", 'a' },
		{ "dear", 'd' },
	};

	std::cout << "Choose popular prices(average, above-average, dear): ";
	std::string tmp;
	std::cin >> tmp;

	auto i = prices.find(tmp);
	if(end(prices) == i)
		throw std::exception("Format error, options: average, above-average, dear");

	switch(i->second) {
	case 'm':
		high = true;
		highValue = 2000;
		break;
	case 'a':
		low = true;
		lowValue = 2000;
		high = true;
		highValue = 4000;
		break;
	case 'd':
		low = true;
		lowValue = 4000;
		break;
	}

	std::cout << std::endl;
}

static void GetFretBoundaries(bool& low, int& lowValue, bool& high, int& highValue) {
	std::string tmp;
	std::cout << "Choose low boundary fret(y/n): ";
	std::cin >> tmp;
	if (tmp == "y") {
		std::cout << "Low fret boundary: ";
		low = true;
		std::cin >> lowValue;
	}

	std::cout << "Choose high boundary fret(y/n): ";
	std::cin >> tmp;
	if (tmp == "y") {
		std::cout << "High fret boundary: ";
		high = true;
		std::cin >> highValue;
	}

	std::cout << std::endl;
}

static IFilterPtr GetFretFilter(bool low, int lowValue, bool high, int highValue) {
	return std::make_shared<FretFilter>(low, lowValue, high, highValue);
}

std::vector<GuitarItem> Menu::FilterSystemStart()
{
	bool end = false;

	Filters filters;

	while (!end)
	{
		std::cout
			<< "1. Type Filter\n"
			<< "2. Price Filter\n"
			<< "3. Popular price\n"
			<< "4. Fret Filter\n"
			<< "5. Tuner Filter\n"
			<< "6. Country Filter\n"
			<< "0. End\n";

		std::cout << std::endl << "Add filter: ";
		int choice;

		std::cin >> choice;
		switch (choice)
		{
		case 0:
			end = true;
			break;
		case 1:
			PrintTypes();
			std::cout << "\nYour choise: ";
			std::cin >> choice;
			filters.push_back(GetTypesFilter(choice));
			break;
		case 2: {
			bool low = false;
			bool high = false;
			int lowPrice = 0;
			int highPrice = 0;
			GetPriceBoundaries(low, lowPrice, high, highPrice);
			filters.push_back(Filter<PirceFilter>(low, lowPrice, high, highPrice));
			break;
		}
		case 3: {
			bool low = false;
			bool high = false;
			int lowPrice = 0;
			int highPrice = 0;
			GetPopularPriceBoundaries(low, lowPrice, high, highPrice);
			filters.push_back(Filter<PirceFilter>(low, lowPrice, high, highPrice));
			break;
		}
		case 4:	{
			bool low = false;
			bool high = false;
			int lowPrice = 0;
			int highPrice = 0;
			GetFretBoundaries(low, lowPrice, high, highPrice);
			filters.push_back(GetFretFilter(low, lowPrice, high, highPrice));
			break;
		}
		case 5:
			filters.push_back(Filter<HasTunerFilter>());
			break;
		case 6: {
			std::string country;
			std::cout << "Enter country: ";
			std::cin >> country;
			filters.push_back(Filter<CountryFilter>(country));
			break;
		}
		default:
			std::cout << "WTF!!!" << std::endl;
			continue;
		}
	}

	

	return FilterItems(m_items, filters);
}

void Menu::OutputItems(const std::vector<GuitarItem>& items)
{
	size_t i = 0;
	for (const auto& item : items)
	{
		std::cout << std::left << std::setw(5) << item.id << item << "\n";
	}
	std::cout << std::endl;
}

void Menu::OutputItemsByIdList(const std::vector<int>& ids)
{
	for (const auto& id : ids)
	{
		std::cout << std::left << std::setw(5) << id << m_items[id] << "\n";
	}
	std::cout << std::endl;
}
