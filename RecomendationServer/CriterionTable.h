#pragma once

#include <vector>

#include "ICriterion.h"

struct CriterionRow {
    int itemId;
    std::vector<ICriterionPtr> criterions;

    CriterionRow(int _itemId, std::vector<ICriterionPtr> _criterions)
        : itemId(_itemId)
        , criterions(_criterions) {};
};

using CriterionTable = std::vector<CriterionRow>;
using CriterionTablePtr = std::shared_ptr<CriterionTable>;