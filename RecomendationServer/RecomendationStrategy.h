#pragma once

#include <set>

#include "IRecomendationStrategy.h"

class RecomendationStrategy : public IRecomendationStrategy {
public:
    RecomendationStrategy() = default;

    virtual std::vector<int> GetRecomendationList(int itemId, const CriterionTablePtr& CTable) const override;
    virtual std::vector<int> GetRecomendationList(const std::vector<int>& likedIds, const std::vector<int>& disLikedIds, const CriterionTablePtr& CTable) override;
    virtual std::vector<double> GetUndorderedRateList(int itemId, const CriterionTablePtr& CTable) const override;

    ~RecomendationStrategy() = default;
private:
    std::set<std::pair<int, double>> GetEachRank(int itemId, const CriterionTablePtr& CTable) const;
};