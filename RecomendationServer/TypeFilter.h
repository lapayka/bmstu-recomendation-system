#pragma once

#include "IFilter.h"

class TypeFilter : public IFilter
{
public:
	TypeFilter(GuitarItem::Shape shape)
		: m_shape(shape) {}

	virtual bool Check(const GuitarItem& item) const override;

	virtual ~TypeFilter() = default;
private:
	GuitarItem::Shape m_shape;
};

