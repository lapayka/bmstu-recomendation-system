// RecomendationSystem.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <iomanip>


#include <boost/di.hpp>
#include <filesystem>

#include "RecomendationStrategy.h"
#include "CCriterionTableBuilder.h"
#include <logger/SpdLogger.h>
#include <config/yamlcpp_config.h>
#include "NetLibGateWay/NetLibGateWay.h"
#include "CGuitarBuilder.h"


#include <CServerHandler.h>
#include <NetLibServerHandlerCreator.h>
#include <NetLibGateWay.h>
#include <NetLibServerHandlerCreator.h>
#include <base/BeastResponseFactory.h>
#include <base/server_req_handler.h>
#include <config/yamlcpp_config.h>
#include <connections/server_connection.h>
#include <logger/LoggerFactory.h>
#include <sessions/http_session.h>

#include "IFilter.h"

#include "Menu.h"

namespace di = boost::di;

std::vector<GuitarItem> items;

int main(int argc, char *argv[])
{
    if (argc < 2) {
        std::cout << "Not enough arguments" << std::endl;
        return EXIT_FAILURE;
    }

    std::filesystem::path logFile(argv[1]);
    if (!std::filesystem::exists(logFile)) {
        std::cout << logFile << " doesn't exists" << std::endl;
        return EXIT_FAILURE;
    }

    asio::io_context ioc;

    auto injector = di::make_injector(
        di::bind<std::string>.named(configFileName).to(logFile.string()),
        di::bind<IRecomendationStrategy>().to<RecomendationStrategy>(),
        di::bind<ITableBuilder>().to<CGuitarBuilder>(),
        di::bind<ICriterionTableBuilder>().to<CCriterionTableBuilder>(),

        di::bind<BaseConfig>().to<YamlCppConfig>(),

        di::bind<IResponseFactory>().to<BeastResponseFactory>(), di::bind<asio::io_context>.named(ioContext).to(ioc), 
        di::bind<IServerReqHandlerCreator>().to<NetLibServerReqHandlerCreator>(),
        di::bind<IServerSessionCreator>().to<HttpServerSessionCreator>(), di::bind<IServerConnection>().to<ServerConnection>()
    );

    {
        ITableBuilderPtr tableBuilder = injector.create<ITableBuilderPtr>();
        items = tableBuilder->BuildGuitarItems(std::filesystem::path("C:\\Users\\Denis\\Desktop\\bmstu\\M_1\\ai_systems\\RecomendationSystem\\RecomendationServer\\dataset.txt"));
    }

    auto path = std::filesystem::current_path();

    LoggerFactory::InitLogger(injector.create<BaseConfigPtr>());
    auto gateWay = injector.create<std::shared_ptr<NetLibGateWay>>();
    gateWay->Run();
}
