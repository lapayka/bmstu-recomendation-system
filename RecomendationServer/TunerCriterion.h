#pragma once
#include "ICriterion.h"

class TunerCriterion :
    public ICriterion
{
public:
    TunerCriterion(bool hasTuner, double weight) : m_weight(weight), m_hasTuner(hasTuner) {}

    virtual double GetDistance(const ICriterionPtr& rCriterion) const override;
    virtual double GetWeight() const override { return m_weight; }

    virtual ~TunerCriterion() = default;

protected:
    virtual std::string GetName() const { return m_name; }

private:
    bool m_hasTuner;
    double m_weight = 1.;
    std::string m_name = "TunerCriterion";
};

using TunerCriterionPtr = std::shared_ptr<TunerCriterion>;