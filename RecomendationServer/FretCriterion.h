#pragma once
#include "ICriterion.h"

class FretCriterion :
    public ICriterion
{
public:
    FretCriterion(int fretCount, double weight) : m_weight(weight), m_fretCount(fretCount) {}

    virtual double GetDistance(const ICriterionPtr& rCriterion) const override;
    virtual double GetWeight() const override { return m_weight; }

    virtual ~FretCriterion() = default;

protected:
    virtual std::string GetName() const {return m_name;}

private:
    int m_fretCount;
    double m_weight = 1.;
    std::string m_name = "FretCriterion";
};

using FretCriterionPtr = std::shared_ptr<FretCriterion>;
