#pragma once
#include "ITableBuilder.h"

class CGuitarBuilder :
    public ITableBuilder
{
public:
	virtual std::vector<GuitarItem> BuildGuitarItems(const std::filesystem::path& file_path) const override;

private:
	GuitarItem BuildGuitarItem(const std::string& csv_string) const;
};

