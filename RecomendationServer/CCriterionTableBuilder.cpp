#include "CCriterionTableBuilder.h"

#include <functional>

#include "FretCriterion.h"
#include "CountryCriterion.h"
#include "PriceCriterion.h"
#include "ShapeCriterion.h"
#include "TunerCriterion.h"
#include "TypeCriterion.h"
#include "MaterialCriterion.h"

ICriterionPtr FretCriterionFabric(const GuitarItem& item)
{
	return std::make_shared<FretCriterion>(item.fretCount, 1.);
}

ICriterionPtr CountryCriterionFabric(const GuitarItem& item)
{
	return std::make_shared<CountryCriterion>(item.country, 1.);
}

ICriterionPtr PriceCriterionFabric(const GuitarItem& item)
{
	return std::make_shared<PriceCriterion>(item.price, 1.);
}

ICriterionPtr ShapeCriterionFabric(const GuitarItem& item)
{
	return std::make_shared<ShapeCriterion>(item.shape, 1.);
}

ICriterionPtr TypeCriterionFabric(const GuitarItem& item)
{
	return std::make_shared<TypeCriterion>(item.type, 1.);
}

ICriterionPtr TunerCriterionFabric(const GuitarItem& item)
{
	return std::make_shared<TunerCriterion>(item.hasTuner, 0.5);
}

ICriterionPtr MaterialCriterionFabric(const GuitarItem& item)
{
	return std::make_shared<MaterialCriterion>(item.material, 1.);
}

CriterionTablePtr CCriterionTableBuilder::BuildCriterionTable(const std::vector<GuitarItem>& items) const
{
	static std::vector<std::function<ICriterionPtr(const GuitarItem&)>> fabricVectors = {
		FretCriterionFabric, CountryCriterionFabric, PriceCriterionFabric, ShapeCriterionFabric, TypeCriterionFabric, TunerCriterionFabric, MaterialCriterionFabric
	};

	CriterionTablePtr res = std::make_shared<CriterionTable>();
	res->reserve(items.size());

	for (const auto& item : items)
	{
		std::vector<ICriterionPtr> criterions;
		criterions.reserve(fabricVectors.size());
		for (const auto& fabric : fabricVectors)
		{
			criterions.push_back(fabric(item));
		}
		res->push_back(CriterionRow(static_cast<int>(item.id), criterions));
	}

	return res;
}
