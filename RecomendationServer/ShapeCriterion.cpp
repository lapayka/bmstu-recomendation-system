#include "ShapeCriterion.h"

#include "TypeTree.h"

double ShapeCriterion::GetDistance(const ICriterionPtr& rCriterion) const
{
    if (auto r_ptr = dynamic_cast<ShapeCriterion*>(rCriterion.get()))
    {
        return 1 - TypeTreeNode::Instance()->GetDistance(m_shape, r_ptr->m_shape) / 5.;
    }
    else {
        throw;
    }

	return 1;
}
