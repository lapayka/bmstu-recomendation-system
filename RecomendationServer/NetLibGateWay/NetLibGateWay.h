#pragma once

#include <connections/IServerConnection.h>
#include <router/router.h>

class NetLibGateWay {
public:
    NetLibGateWay(const IServerConnectionPtr& serverConnection);

    void Run();

    virtual ~NetLibGateWay() = default;

private:
    void SetupRouter();

    IServerConnectionPtr m_serverConnection;
    RequestsRouterPtr m_router;
};