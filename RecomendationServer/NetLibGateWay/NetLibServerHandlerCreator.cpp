#include "NetLibServerHandlerCreator.h"

#include "CServerHandler.h"

NetLibServerReqHandlerCreator::NetLibServerReqHandlerCreator(const IResponseFactoryPtr& fact)
    : m_fact(fact)
{
}

std::shared_ptr<IServerReqHandler> NetLibServerReqHandlerCreator::CreateHandler() const { return std::make_shared<CServerHandler>(m_fact); }