#pragma once

#include <base/IResponseFactory.h>
#include <base/server_req_handler.h>
#include <config/base_config.h>
#include <router/router.h>

class CServerHandler : public IServerReqHandler {
public:
    CServerHandler(const IResponseFactoryPtr& respFactory);

    virtual void HandleRequest(const std::shared_ptr<IRequest>& req) override;
    virtual void MakeResponse(const std::shared_ptr<IResponse>& resp) override;

    virtual ~CServerHandler() = default;

private:
    RequestsRouterPtr m_router;

    IResponseFactoryPtr m_respFactory;
    IResponsePtr m_resp;
};