#pragma once

#include <base/IResponseFactory.h>
#include <base/server_req_handler.h>

class NetLibServerReqHandlerCreator : public IServerReqHandlerCreator {
public:
    NetLibServerReqHandlerCreator(const IResponseFactoryPtr&);

    virtual std::shared_ptr<IServerReqHandler> CreateHandler() const override;

    virtual ~NetLibServerReqHandlerCreator() = default;

private:
    IResponseFactoryPtr m_fact;
};