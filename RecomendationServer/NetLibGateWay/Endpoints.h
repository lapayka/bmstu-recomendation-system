#pragma once

#include <base/reqresp.h>

// static endpoints
void GetAllGuitars(const IResponsePtr&, const IRequestPtr&);
void GetRecomendations(const IResponsePtr&, const IRequestPtr&);
void GetFiltrations(const IResponsePtr&, const IRequestPtr&);
