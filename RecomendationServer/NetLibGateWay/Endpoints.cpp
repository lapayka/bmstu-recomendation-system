#include "Endpoints.h"

#include <logger/LoggerFactory.h>
#include "../RecomendationStrategy.h"

#include <sstream>

#include "DTO.h"

extern std::vector<GuitarItem> items;

void GetStatus(const IResponsePtr& resp, const IRequestPtr&)
{
    resp->SetBody("Server is ok");
    resp->SetStatus(net::CODE_200);
}

void GetAllGuitars(const IResponsePtr& resp, const IRequestPtr& req)
{
    std::vector<GuitarItem> guitars;
    resp->SetBody(GuitarsToDTO(guitars));
    resp->SetStatus(net::CODE_200);
}

void GetRecomendations(const IResponsePtr& resp, const IRequestPtr& req)
{
    std::vector<GuitarItem> guitars;
    resp->SetBody(GuitarsToDTO(guitars));
    resp->SetStatus(net::CODE_200);
}

static std::vector<GuitarItem> FilterItems(const std::vector<GuitarItem>& items, const Filters& filters)
{
	std::vector<GuitarItem> res;

	for (const auto& item : items)
	{
		bool check = true;

		for (const auto& filter : filters)
		{
			if (!filter->Check(item))
			{
				check = false;
				break;
			}
		}

		if (check)
			res.push_back(item);
	}

	return res;
}

void GetFiltrations(const IResponsePtr& resp, const IRequestPtr& req)
{
    auto filters = DTOToFilters(req->GetBody());

	std::vector<GuitarItem> guitars = FilterItems(items, filters);
    
    resp->SetBody(GuitarsToDTO(guitars));
    resp->SetStatus(net::CODE_200);
}
