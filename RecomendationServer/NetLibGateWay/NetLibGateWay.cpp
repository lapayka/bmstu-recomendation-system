#include "NetLibGateWay.h"

#include "Endpoints.h"
#include "logger/LoggerFactory.h"

NetLibGateWay::NetLibGateWay(const IServerConnectionPtr& serverConnection)
    : m_serverConnection(serverConnection)
    , m_router(RequestsRouter::Instanse())
{
}

void NetLibGateWay::SetupRouter()
{
    m_router->AddStaticEndpoint({ "/api/v1/guitars", net::GET }, GetAllGuitars);
    m_router->AddStaticEndpoint({ "/api/v1/guitars/liked", net::GET }, GetRecomendations);
    m_router->AddStaticEndpoint({ "/api/v1/guitars/filtered", net::POST }, GetFiltrations);
}

void NetLibGateWay::Run()
{
    LoggerFactory::GetLogger()->LogInfo("Server started");

    SetupRouter();
    m_serverConnection->Run();
}
