#include "DTO.h"
#include "../PirceFilter.h"
#include "../CountryFilter.h"
#include "../FretFilter.h"

#include <json/json.h>

Json::Value GuitarToJson(const GuitarItem& guitar)
{
    Json::Value root;

    Json::Value data;
    data["id"] = guitar.id;
    data["name"] = guitar.name;
    data["price"] = guitar.price;
    data["fretCount"] = guitar.fretCount;
    data["shape"] = shapeToStringMap[guitar.shape];
    data["hasTuner"] = guitar.hasTuner;
    data["country"] = guitar.country;
    data["material"] = guitar.material;

    root = data;
    return root;
}

std::string GuitarToDTO(const GuitarItem& user)
{
    Json::FastWriter writer;
    return writer.write(GuitarToJson(user));
}

std::string GuitarsToDTO(const std::vector<GuitarItem>& users)
{
    Json::Value root;

    Json::Value data = Json::arrayValue;

    for (const auto& user : users) {
        data.append(GuitarToJson(user));
    }

    root = data;
    Json::FastWriter writer;
    return writer.write(root);
}

std::vector<IFilterPtr> DTOToFilters(const std::string& str)
{
    Json::Value data;
    Json::Reader reader;
    reader.parse(str, data);

    std::vector<IFilterPtr> res;
    if (!data["price"].empty())
        res.push_back(std::make_shared<PirceFilter>(false, 0, true, data["price"].asInt()));
    if (!data["country"].empty())
        res.push_back(std::make_shared<CountryFilter>(data["country"].asCString()));
    if (!data["fret"].empty())
        res.push_back(std::make_shared<FretFilter>(true, data["fret"].asInt(), false, 0));

    return res;
}