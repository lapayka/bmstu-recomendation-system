#include "CServerHandler.h"

#include <logger/LoggerFactory.h>

#include <router/router.h>

CServerHandler::CServerHandler(const IResponseFactoryPtr& respFactory)
    : m_router(RequestsRouter::Instanse())
    , m_respFactory(respFactory)
{
}

void CServerHandler::HandleRequest(const std::shared_ptr<IRequest>& req)
{
    static_req_handler_t route = m_router->RouteReq(req->GetTarget(), req->GetMethod());

    m_resp = m_respFactory->CreateRespone();

    try {
        route(m_resp, req);
    } catch (const std::exception& e) {
        LoggerFactory::GetLogger()->LogError(e.what());
        m_resp->SetStatus(net::CODE_503);
    }
}

void CServerHandler::MakeResponse(const std::shared_ptr<IResponse>& resp) { resp->copy(m_resp); }
