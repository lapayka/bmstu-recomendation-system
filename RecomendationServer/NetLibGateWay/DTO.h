#pragma once

#include "../GuitarItem.h"
#include "../IFilter.h"

std::string GuitarToDTO(const GuitarItem& user);
std::string GuitarsToDTO(const std::vector<GuitarItem>& user);
std::vector<IFilterPtr> DTOToFilters(const std::string& str);