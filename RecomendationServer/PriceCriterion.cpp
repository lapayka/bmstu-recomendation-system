#include "PriceCriterion.h"

double PriceCriterion::GetDistance(const ICriterionPtr& rCriterion) const
{
    if (auto r_ptr = dynamic_cast<PriceCriterion*>(rCriterion.get()))
    {
        int max = std::max(m_price, r_ptr->m_price);
        return (max - static_cast<double>(abs(m_price - r_ptr->m_price))) / max;
    }
    else {
        throw;
    }
    return 0.;
}
