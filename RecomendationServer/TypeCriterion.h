#pragma once
#include "ICriterion.h"
#include "GuitarItem.h"

class TypeCriterion :
    public ICriterion
{
public:
    TypeCriterion(GuitarItem::Type type, double weight) : m_weight(weight), m_type(type) {}

    virtual double GetDistance(const ICriterionPtr& rCriterion) const override;
    virtual double GetWeight() const override { return m_weight; }

    virtual ~TypeCriterion() = default;

protected:
    virtual std::string GetName() const {return m_name;}

private:
    GuitarItem::Type m_type;
    double m_weight = 1.;
    std::string m_name = "TypeCriterion";
};

using TypeCriterionPtr = std::shared_ptr<TypeCriterion>;