#pragma once
#include "ICriterion.h"

class PriceCriterion :
    public ICriterion
{
public:
    PriceCriterion(int price, double weight) : m_weight(weight), m_price(price) {}

    virtual double GetDistance(const ICriterionPtr& rCriterion) const override;
    virtual double GetWeight() const override { return m_weight; }

    virtual ~PriceCriterion() = default;

protected:
    virtual std::string GetName() const {return m_name;}

private:
    int m_price;
    double m_weight = 1.;
    std::string m_name = "PriceCriterion";
};

using PriceCriterionPtr = std::shared_ptr<PriceCriterion>;