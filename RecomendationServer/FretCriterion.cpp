#include "FretCriterion.h"

double FretCriterion::GetDistance(const ICriterionPtr& rCriterion) const
{
    if (auto r_ptr = dynamic_cast<FretCriterion*>(rCriterion.get()))
    {
        return (5 - static_cast<double>(abs(m_fretCount - r_ptr->m_fretCount))) / 5;
    }
    else {
        throw;
    }
    return 0.;
}
