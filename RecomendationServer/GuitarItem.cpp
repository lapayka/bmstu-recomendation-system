#include "GuitarItem.h"

#include <iomanip>

std::ostream& operator<<(std::ostream& str, const GuitarItem &item)
{
	str << std::left << std::setw(60) << item.name << " "
		<< std::setw(6) << item.price << " " 
		<< std::setw(4) << item.fretCount << " " 
		<< std::setw(3) << item.hasTuner << " " 
		<< std::setw(15) << shapeToStringMap[item.shape] << " " 
		<< std::setw(10) << item.country << " "
		<< std::setw(15) << item.material;

	return str;
}
