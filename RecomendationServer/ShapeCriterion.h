#pragma once
#include "ICriterion.h"
#include "GuitarItem.h"

class ShapeCriterion :
    public ICriterion
{
public:
    ShapeCriterion(GuitarItem::Shape shape, double weight) : m_weight(weight), m_shape(shape) {}

    virtual double GetDistance(const ICriterionPtr& rCriterion) const override;
    virtual double GetWeight() const override { return m_weight; }

    virtual ~ShapeCriterion() = default;

protected:
    virtual std::string GetName() const {return m_name;}

private:
    GuitarItem::Shape m_shape;
    double m_weight = 1.;
    std::string m_name = "ShapeCriterion";
};

using ShapeCriterionPtr = std::shared_ptr<ShapeCriterion>;
