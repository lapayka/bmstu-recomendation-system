#pragma once

#include <string>
#include <iostream>
#include <unordered_map>
#include <map>

class GuitarItem
{
public:
	enum Shape {
		s_ROOT,

		s_Electric,
		s_SuperStrat,
		s_Strat,
		s_FlyingV,
		s_LesPaul,
		s_Telecaster,
		s_Mustang,
		s_ES45,

		s_Acustic,

		s_SteelAcustic,
		s_Concert,
		s_Dreadnout,

		s_NeulonAcustic,
		s_Classic,
		s_Count
	};
	enum Type {
		t_Electric,
		t_EJazz,
		t_Classic,
		t_Dreadnout,
		t_Concert,
		t_Count
	};

	std::string name;
	size_t id;
	int price;
	int fretCount;
	Type type;
	Shape shape;
	bool hasTuner;
	std::string country;
	std::string material;
};


static std::map<std::string, GuitarItem::Shape> stringToShapeMap = {
			{"All", GuitarItem::s_ROOT},

			{"Electric", GuitarItem::s_Electric},
			{"Superstrat", GuitarItem::s_SuperStrat},
			{"Stratocaster", GuitarItem::s_Strat},
			{"Mustang", GuitarItem::s_Mustang},
			{"Flying_V", GuitarItem::s_FlyingV},
			{"ES45", GuitarItem::s_ES45},
			{"Concert", GuitarItem::s_Concert},
			{"Telecaster", GuitarItem::s_Telecaster},
			{"LesPaul", GuitarItem::s_LesPaul},

			{"Acoustic", GuitarItem::s_Acustic},

			{"Steel string", GuitarItem::s_SteelAcustic},
			{"Dreadnout", GuitarItem::s_Dreadnout},

			{"Neulon string", GuitarItem::s_NeulonAcustic},
			{"Classic", GuitarItem::s_Classic},
};

static std::vector<GuitarItem::Shape> filtersArray = {
			{ GuitarItem::s_ROOT }, { GuitarItem::s_Electric }, { GuitarItem::s_SuperStrat },
			{ GuitarItem::s_Strat }, { GuitarItem::s_Mustang }, { GuitarItem::s_FlyingV },
			{ GuitarItem::s_ES45 }, { GuitarItem::s_Concert }, { GuitarItem::s_Telecaster },
	        { GuitarItem::s_LesPaul }, { GuitarItem::s_Acustic }, { GuitarItem::s_SteelAcustic },
			{ GuitarItem::s_Dreadnout }, { GuitarItem::s_NeulonAcustic }, { GuitarItem::s_Classic },
};

static std::map<GuitarItem::Shape, std::string> shapeToStringMap = {
			{GuitarItem::s_ROOT, "All"},

			{GuitarItem::s_Electric, "Electric"},
			{GuitarItem::s_SuperStrat, "Superstrat"},
			{GuitarItem::s_Strat, "Stratocaster"},
			{GuitarItem::s_Mustang, "Mustang"},
			{GuitarItem::s_FlyingV, "Flying_V"},
			{GuitarItem::s_ES45, "ES45"},
			{GuitarItem::s_Concert, "Concert"},
			{GuitarItem::s_Telecaster, "Telecaster"},
			{GuitarItem::s_LesPaul, "LesPaul"},

			{GuitarItem::s_Acustic, "Acoustic"},

			{GuitarItem::s_SteelAcustic, "Steel string"},
			{GuitarItem::s_Dreadnout, "Dreadnout"},

			{GuitarItem::s_NeulonAcustic, "Neulon string"},
			{GuitarItem::s_Classic, "Classic"},
};

static std::unordered_map<std::string, GuitarItem::Type> stringToTypeMap = {
			{"Electric", GuitarItem::t_Electric},
			{"EJazz", GuitarItem::t_EJazz},
			{"Concert", GuitarItem::t_Concert},
			{"Dreadnout", GuitarItem::t_Dreadnout},
			{"Classic", GuitarItem::t_Classic},
};

static std::unordered_map<GuitarItem::Type, std::string> typeToStringMap = {
			{GuitarItem::t_Electric, "Electric"},
			{GuitarItem::t_EJazz, "EJazz"},
			{GuitarItem::t_Concert, "Concert"},
			{GuitarItem::t_Dreadnout, "Dreadnout"},
			{GuitarItem::t_Classic, "Classic"},
};

std::ostream& operator <<(std::ostream& str, const GuitarItem &item);