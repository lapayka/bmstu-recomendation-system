#include "RecomendationStrategy.h"

#include <set>
#include <algorithm>
#include <boost/format.hpp>
#include <boost/range/combine.hpp>

static void insert_sorted(std::set<std::pair<double, int>>& vec, const std::pair<double, int>& val)
{
    vec.insert(val);

    return;
}

std::vector<int> RecomendationStrategy::GetRecomendationList(int itemId, const CriterionTablePtr& CTable) const
{
    auto itemIdIter = std::find_if(CTable->begin(), CTable->end(), [itemId](const CriterionRow& cr) { return cr.itemId == itemId; });

    if (itemIdIter == CTable->end() || itemIdIter->itemId != itemId) {
        throw std::logic_error(boost::str(boost::format("Criterion table doesn't contain user (id: %1%)") % itemId));
    }

    std::set<std::pair<double, int>> res;

    for (const auto& criterionRow : *CTable) {
        if (criterionRow.itemId != itemId) {
            double sum = 0.;
            double den = 0;

            for (size_t i = 0; i < criterionRow.criterions.size() && i < itemIdIter->criterions.size(); i++) {
                den += itemIdIter->criterions[i]->GetWeight();

                double crt_tmp = itemIdIter->criterions[i]->GetDistance(criterionRow.criterions[i]);

                sum += crt_tmp;
            }

            insert_sorted(res, std::make_pair(sum / den, criterionRow.itemId));
        }
    }

    std::vector<int> ret;
    for (auto riter = res.rbegin(); riter != res.rend(); riter++)
        ret.push_back(riter->second);

    return ret;
}

std::vector<int> RecomendationStrategy::GetRecomendationList(const std::vector<int>& likedIds, const std::vector<int>& disLikedIds, const CriterionTablePtr& CTable)
{
    std::vector<std::vector<std::pair<int, double>>> liked_res;
    std::vector<std::vector<std::pair<int, double>>> disliked_res;

    auto translate = [](const std::set<std::pair<int, double>>& set)
        {
            std::vector<std::pair<int, double>> vec;
            vec.assign(set.begin(), set.end());

            return vec;
        };

    for (const auto& likedId : likedIds)
    {
        liked_res.push_back(translate(GetEachRank(likedId, CTable)));
    }

    for (const auto& disLikedId : disLikedIds)
    {
        disliked_res.push_back(translate(GetEachRank(disLikedId, CTable)));
    }

    std::vector < std::pair< int, double >> reducedLikedRes;
    for (size_t i = 0; i < CTable->size(); i++)
        reducedLikedRes.push_back(std::make_pair(static_cast<int>(i), 0.));

    if (!liked_res.empty())
    {
        for (size_t i = 0; i < liked_res[0].size(); i++)
        {
            for (size_t j = 0; j < liked_res.size(); j++)
            {
                reducedLikedRes[i].second += liked_res[j][i].second;
            }
            reducedLikedRes[i].second /= liked_res.size();
        }
    }

    std::vector < std::pair< int, double >> reducedDisLikedRes;
    for (size_t i = 0; i < CTable->size(); i++)
        reducedDisLikedRes.push_back(std::make_pair(static_cast<int>(i), 0.));

    if (!disliked_res.empty())
    {
        for (size_t i = 0; i < disliked_res[0].size(); i++)
        {
            for (size_t j = 0; j < disliked_res.size(); j++)
            {
                reducedDisLikedRes[i].second += disliked_res[j][i].second;
            }
            reducedDisLikedRes[i].second /= disliked_res.size();
        }
    }

    std::set<std::pair<double, int>> res;

    for (size_t i = 0; i < reducedDisLikedRes.size(); i++)
    {
        res.insert(std::make_pair(reducedLikedRes[i].second - reducedDisLikedRes[i].second, reducedDisLikedRes[i].first));
    }

    std::vector<int> _res;
    for (auto iter = res.rbegin(); iter != res.rend(); ++iter)
    {
        _res.push_back(iter->second);
    }

    return _res;
}

std::vector<double> RecomendationStrategy::GetUndorderedRateList(int itemId, const CriterionTablePtr& CTable) const
{
    auto itemIdIter = std::find_if(CTable->begin(), CTable->end(), [itemId](const CriterionRow& cr) { return cr.itemId == itemId; });

    if (itemIdIter == CTable->end() || itemIdIter->itemId != itemId) {
        throw std::logic_error(boost::str(boost::format("Criterion table doesn't contain user (id: %1%)") % itemId));
    }

    std::vector<std::pair<int, double>> res; // todo change to set

    for (const auto& criterionRow : *CTable) {
        double sum = 0.;
        double den = 0;

        for (size_t i = 0; i < criterionRow.criterions.size() && i < itemIdIter->criterions.size(); i++) {
            den += itemIdIter->criterions[i]->GetWeight();

            double crt_tmp = itemIdIter->criterions[i]->GetDistance(criterionRow.criterions[i]);

            sum += crt_tmp;
        }

        res.push_back(std::make_pair(criterionRow.itemId, sum / den));
    }

    std::vector<double> ret;

    for (const auto &pair : res)
        ret.push_back(pair.second);

    return ret;
}

std::set<std::pair<int, double>> RecomendationStrategy::GetEachRank(int itemId, const CriterionTablePtr& CTable) const
{
    auto itemIdIter = std::find_if(CTable->begin(), CTable->end(), [itemId](const CriterionRow& cr) { return cr.itemId == itemId; });
    std::set<std::pair<int, double>> res;

    for (const auto& criterionRow : *CTable) {
        double sum = 0.;
        double den = 0;

        for (size_t i = 0; i < criterionRow.criterions.size() && i < itemIdIter->criterions.size(); i++) {
            den += itemIdIter->criterions[i]->GetWeight();

            double crt_tmp = itemIdIter->criterions[i]->GetDistance(criterionRow.criterions[i]);

            sum += crt_tmp;
        }

        res.insert(std::make_pair(criterionRow.itemId, sum / den));
    }

    return res;
}

