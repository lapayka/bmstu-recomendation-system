#include "PirceFilter.h"

bool PirceFilter::Check(const GuitarItem& item) const
{
    bool rc = true;

    if (m_is_low_strip)
        rc = item.price >= m_low_strip;

    if (rc && m_is_high_strip)
        rc = item.price <= m_high_strip;

    return rc;
}
