#pragma once
#include <vector>
#include "GuitarItem.h"
#include "IRecomendationStrategy.h"
#include "ICriterionTableBuilder.h"

class Menu
{
public:
	Menu(const std::vector<GuitarItem>& items, const IRecomendationStrategyPtr& rec, const ICriterionTableBuilderPtr& crit)
		:
		m_items(items),
		m_recStrategy(rec),
		m_criterionBuilder(crit)
	{}

	void start();

private:
	std::vector<int> RecomendationSystemStart();
	std::vector<int> RecomendationSystemStart(const std::vector< GuitarItem> &items);
	std::vector<GuitarItem> FilterSystemStart();

	void OutputItems(const std::vector<GuitarItem>& items);
	void OutputItemsByIdList(const std::vector<int>& ids);

	std::vector<GuitarItem> m_items;
	IRecomendationStrategyPtr m_recStrategy;
	ICriterionTableBuilderPtr m_criterionBuilder;
};

