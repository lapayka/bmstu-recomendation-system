#include "MaterialCriterion.h"

#include <set>
#include <sstream>
#include <algorithm>

std::set<std::string> getTokens(const std::string& str)
{
    std::set<std::string> tokens;
    std::string token;
    std::stringstream stream(str);
    while (std::getline(stream, token, '_'))
    {
        tokens.insert(token);
    }

    return tokens;
}


double MaterialCriterion::GetDistance(const ICriterionPtr& rCriterion) const
{
    if (auto r_ptr = dynamic_cast<MaterialCriterion*>(rCriterion.get()))
    {
        std::set<std::string> lhs(getTokens(m_material)), rhs(getTokens(r_ptr->m_material));

        size_t match_count = 0;
        for (const auto& token : lhs)
        {
            auto iter = rhs.find(token);
            match_count += iter != rhs.end();
        }

        return double(match_count * 2) / (lhs.size() + rhs.size());
    }
    else {
        throw;
    }
    return 0.;
}
