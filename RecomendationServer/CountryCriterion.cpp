#include "CountryCriterion.h"
#include <unordered_map>
#include <functional>

double USA(const std::string& str)
{
    if (str == "Mexico")
        return 0.75;
    else if (str == "Japan")
        return 0.5;

    return 0;
}

double Mexico(const std::string& str)
{
    if (str == "USA")
        return 0.75;
    else if (str == "Japan")
        return 0.25;

    return 0;
}

double Japan(const std::string& str)
{
    if (str == "Mexico")
        return 0.25;
    else if (str == "USA")
        return 0.5;

    return 0;
}

double CountryCriterion::GetDistance(const ICriterionPtr& rCriterion) const
{
    std::unordered_map < std::string, std::function<double(const std::string&)>> map = {
        {"USA", USA},
        {"Japan", Japan},
        {"Mexico", Mexico}
    };

    if (auto r_ptr = dynamic_cast<CountryCriterion*>(rCriterion.get()))
    {
        if (m_country == r_ptr->m_country)
            return 1.0;

        auto iter = map.find(m_country);
        if (iter == map.end())
        {
            _ASSERT(0);
            return 0;
        }

        return iter->second(r_ptr->m_country);
    }
    else {
        throw;
    }
}
