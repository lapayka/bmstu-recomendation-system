#pragma once

#include "IFilter.h"

class PirceFilter : public IFilter
{
public:
	PirceFilter(bool is_low_strip, int low_strip, bool is_high_strip, int high_strip)
		:
		m_is_low_strip(is_low_strip),
		m_low_strip(low_strip),
		m_is_high_strip(is_high_strip),
		m_high_strip(high_strip) {}

	virtual bool Check(const GuitarItem& item) const override;

	virtual ~PirceFilter() = default;
private:
	bool m_is_low_strip = false;
	int m_low_strip = 0;
	bool m_is_high_strip = false;
	int m_high_strip = 0;
};

