#pragma once
#include "ICriterion.h"
class MaterialCriterion :
    public ICriterion
{
public:
    MaterialCriterion(const std::string &material, double weight) : m_weight(weight), m_material(material) {}

    virtual double GetDistance(const ICriterionPtr& rCriterion) const override;
    virtual double GetWeight() const override { return m_weight; }

    virtual ~MaterialCriterion() = default;

protected:
    virtual std::string GetName() const { return m_name; }

private:
    std::string m_material;
    double m_weight = 1.;
    std::string m_name = "MaterialCriterion";
};

