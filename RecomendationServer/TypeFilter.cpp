#include "TypeFilter.h"

#include "TypeTree.h"

bool TypeFilter::Check(const GuitarItem& item) const
{
    auto shapes = TypeTreeNode::Instance()->GetNode(m_shape)->GetShapes();

    auto iter = shapes.find(item.shape);

    return iter != shapes.end();
}
