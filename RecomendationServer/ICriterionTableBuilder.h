#pragma once

#include <vector>

#include "CriterionTable.h"
#include "GuitarItem.h"

class ICriterionTableBuilder
{
public:
	virtual CriterionTablePtr BuildCriterionTable(const std::vector<GuitarItem>& items) const = 0;

	virtual ~ICriterionTableBuilder() = default;
};

using ICriterionTableBuilderPtr = std::shared_ptr<ICriterionTableBuilder>;

