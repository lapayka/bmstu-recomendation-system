#pragma once
#include "IFilter.h"
class CountryFilter :
    public IFilter
{
public:
    CountryFilter(std::string country) : m_country(country) {}

    virtual bool Check(const GuitarItem& item) const override { return item.country == m_country; };

    virtual ~CountryFilter() = default;
private:
    std::string m_country;
};

