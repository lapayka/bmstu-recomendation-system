#include "CGuitarBuilder.h"

#include <sstream>
#include <unordered_map>
#include <fstream>

#include <filesystem>

std::vector<GuitarItem> CGuitarBuilder::BuildGuitarItems(const std::filesystem::path& file_path) const
{
    std::vector<GuitarItem> res;

    std::ifstream stream(file_path.string());

    {
        std::string tmp;
        std::getline(stream, tmp);
    }

    {
        std::string line;
        for (size_t i = 0; std::getline(stream, line); i++)
        {
            auto item = BuildGuitarItem(line);
            item.id = i;
            res.push_back(item);
        }
    }

    return res;
}

GuitarItem CGuitarBuilder::BuildGuitarItem(const std::string& csv_string) const
{
    std::stringstream lineStream(csv_string);

    GuitarItem item;

    lineStream >> item.name;
    lineStream >> item.price;
    lineStream >> item.fretCount;
    {
        std::string type;
        lineStream >> type;

        static_assert(GuitarItem::t_Count == 5, "Error");

        auto iter = stringToTypeMap.find(type);

        if (iter == stringToTypeMap.end())
            throw;

        item.type = iter->second;

    }
    {
        std::string shape;
        lineStream >> shape;

        static_assert(GuitarItem::s_Count == 15, "Error");

        auto iter = stringToShapeMap.find(shape);

        if (iter == stringToShapeMap.end())
            throw;

        item.shape = iter->second;

    }

    {
        std::string hasTuner;
        lineStream >> hasTuner;
        item.hasTuner = hasTuner == "Yes";
    }
    lineStream >> item.country;
    lineStream >> item.material;
    std::transform(item.material.begin(), item.material.end(), item.material.begin(),
        [](unsigned char c) { return std::tolower(c); });

    return item;
}
