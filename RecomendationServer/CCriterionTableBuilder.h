#pragma once
#include "ICriterionTableBuilder.h"
class CCriterionTableBuilder :
    public ICriterionTableBuilder
{
public:
	virtual CriterionTablePtr BuildCriterionTable(const std::vector<GuitarItem>& items) const override;

	virtual ~CCriterionTableBuilder() = default;
};

