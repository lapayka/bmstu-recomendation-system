#include "TunerCriterion.h"

double TunerCriterion::GetDistance(const ICriterionPtr& rCriterion) const
{
    if (auto r_ptr = dynamic_cast<TunerCriterion*>(rCriterion.get()))
    {
        return r_ptr->m_hasTuner == m_hasTuner;
    }
    else {
        throw;
    }
    return 0.;
}
