#include "TypeTree.h"

TypeTreeNodePtr TypeTreeNode::Instance()
{
	std::vector<std::shared_ptr<TypeTreeNode>> electric = {
		std::make_shared<TypeTreeNode>(GuitarItem::s_SuperStrat),
		std::make_shared<TypeTreeNode>(GuitarItem::s_Strat),
		std::make_shared<TypeTreeNode>(GuitarItem::s_FlyingV),
		std::make_shared<TypeTreeNode>(GuitarItem::s_LesPaul),
		std::make_shared<TypeTreeNode>(GuitarItem::s_Telecaster),
		std::make_shared<TypeTreeNode>(GuitarItem::s_Mustang),
		std::make_shared<TypeTreeNode>(GuitarItem::s_ES45),
    };

	std::vector<std::shared_ptr<TypeTreeNode>> steel = {
		std::make_shared<TypeTreeNode>(GuitarItem::s_Concert),
		std::make_shared<TypeTreeNode>(GuitarItem::s_Dreadnout)
	};
	std::vector<std::shared_ptr<TypeTreeNode>> neulon = {
		std::make_shared<TypeTreeNode>(GuitarItem::s_Classic)
	};
	std::shared_ptr<TypeTreeNode> Acoustic = 
		std::make_shared<TypeTreeNode>(std::vector<std::shared_ptr<TypeTreeNode>>{
			std::make_shared<TypeTreeNode>(steel, GuitarItem::s_SteelAcustic) ,
			std::make_shared<TypeTreeNode>(neulon, GuitarItem::s_NeulonAcustic) ,
		}, GuitarItem::s_Acustic);

	TypeTreeNodePtr ElectricPtr = std::make_shared<TypeTreeNode>(electric, GuitarItem::s_Electric);
	static TypeTreeNodePtr RootPtr = std::make_shared<TypeTreeNode>(std::vector <std::shared_ptr<TypeTreeNode>>{ ElectricPtr, Acoustic }, GuitarItem::s_ROOT);

	return RootPtr;
}

std::set<GuitarItem::Shape> TypeTreeNode::GetShapes()
{
	std::set<GuitarItem::Shape> res{m_shape};
	for (const auto& node : m_nodes)
	{
		const auto tmp = node->GetShapes();
		res.insert(tmp.begin(), tmp.end());
	}

	return res;
}

size_t TypeTreeNode::GetDistance(GuitarItem::Shape left, GuitarItem::Shape right)
{
	std::vector<int> way_to_left = GetWay(left);
	std::vector<int> way_to_right = GetWay(right);

	size_t diff = 0;
	for (; diff < way_to_left.size() && diff < way_to_right.size(); diff++)
		if (way_to_left[diff] != way_to_right[diff])
			break;

	return way_to_left.size() + way_to_right.size() - diff * 2;
}

std::shared_ptr<TypeTreeNode> TypeTreeNode::GetNode(GuitarItem::Shape shape)
{
	if (m_shape == shape)
		return std::make_shared<TypeTreeNode>(*this);

	std::shared_ptr<TypeTreeNode> res = nullptr;
	for (const auto& node : m_nodes)
	{
		res = node->GetNode(shape);
		if (res != nullptr)
			return res;
	}

	return res;
}

std::vector<int> TypeTreeNode::GetWay(GuitarItem::Shape shape)
{
	std::vector<int> res;
	
	if (m_shape == shape)
		return res;

	for (size_t i = 0; i < m_nodes.size(); i++)
	{
		std::vector<int> tmp = m_nodes[i]->GetWay(shape);

		if (tmp.empty() || tmp[0] != -1)
		{
			res.push_back(int(i));
			res.insert(res.end(), tmp.begin(), tmp.end());
			return res;
		}
	}

	return { -1 };
}
