#pragma once

#include <memory>
#include <string>

class ICriterion;
using ICriterionPtr = std::shared_ptr<ICriterion>;

class ICriterion {
public:
    virtual double GetDistance(const ICriterionPtr& rCriterion) const = 0;
    virtual double GetWeight() const = 0;

    virtual ~ICriterion() = 0;

protected:
    virtual std::string GetName() const = 0;
};