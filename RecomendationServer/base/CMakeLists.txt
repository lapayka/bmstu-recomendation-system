set( PROJECT_NAME "base" )
project( ${PROJECT_NAME} CXX )

find_package(yaml-cpp REQUIRED)
find_package(spdlog REQUIRED) # spdlog::spdlog

add_subdirectory(logger_test)

add_library( ${PROJECT_NAME} STATIC )

target_sources( ${PROJECT_NAME} PRIVATE
    common_macros.h
    exceptions/base_exception.h
    exceptions/database_exceptions.h
    exceptions/logic_exceptions.h
    exceptions/server_exceptions.h
    config/base_config.h
    config/base_sections.h
    config/yamlcpp_config.h
    config/yamlcpp_config.cpp
    logger/ILogger.h
    logger/SpdLogger.h
    logger/SpdLogger.cpp
    logger/LoggerFactory.h
    logger/LoggerFactory.cpp
)

target_include_directories( ${PROJECT_NAME} 
    PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}
        )
target_link_libraries( ${PROJECT_NAME}
    PUBLIC
        yaml-cpp
        spdlog::spdlog
        Boost::boost
)