#pragma once

#include "GuitarItem.h"

class IFilter
{
public:
	virtual bool Check(const GuitarItem& item) const = 0;

	virtual ~IFilter() = default;
};

using IFilterPtr = std::shared_ptr<IFilter>;

using Filters = std::vector<IFilterPtr>;