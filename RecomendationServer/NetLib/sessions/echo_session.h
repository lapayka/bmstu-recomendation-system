#ifndef ECHO_SESSION_H
#define ECHO_SESSION_H

#include "base_session.h"

class EchoSession
    : public IBaseServerSession
    , public std::enable_shared_from_this<EchoSession> {
public:
    EchoSession() = default;

    virtual std::future<void> Run(tcp::socket sock) override;
};

class EchoSessionCreator : public IServerSessionCreator {
public:
    EchoSessionCreator() = default;
    virtual ~EchoSessionCreator() = default;

    std::shared_ptr<IBaseServerSession> CreateSession() override { return std::shared_ptr<IBaseServerSession>(new EchoSession()); }
};

#endif // ECHO_SESSION_H
