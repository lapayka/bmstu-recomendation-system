#ifndef BASECLIENTSERVERSESSION_H
#define BASECLIENTSERVERSESSION_H

#include <memory>

#include "net.h"

class IClientServerSession {
public:
    virtual ~IClientServerSession() = default;
    virtual std::future<void> Run(tcp::socket server_sock, const std::vector<std::shared_ptr<tcp::socket>>& clients_sock) = 0;
};

class IClientServerSessionCreator {
public:
    virtual ~IClientServerSessionCreator() = default;

    virtual std::shared_ptr<IClientServerSession> CreateSession() = 0;
};

#endif // BASECLIENTSERVERSESSION_H
