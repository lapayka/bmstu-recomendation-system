#ifndef BASE_SESSION_H
#define BASE_SESSION_H

#include <net_headers/net.h>

#include <memory>

class IBaseServerSession {
public:
    virtual ~IBaseServerSession() = default;

    virtual std::future<void> Run(tcp::socket sock) = 0;
};

class IServerSessionCreator {
public:
    virtual ~IServerSessionCreator() = default;

    virtual std::shared_ptr<IBaseServerSession> CreateSession() = 0;
};

#endif // BASE_SESSION_H
