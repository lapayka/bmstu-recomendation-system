#ifndef DETAILSROUTER_H
#define DETAILSROUTER_H

#include <base/reqresp.h>

#include <functional>
#include <map>
#include <memory>
#include <regex>
#include <vector>

struct RequestParams {
    std::string target;
    net::method_t method;

    bool operator<(const RequestParams& params) const
    {
        return target < params.target || (target == params.target && method < params.method);
    }
};

struct RequestParamsRegEx {
    std::regex target;
    net::method_t method;
};

using static_req_handler_t = std::function<void(const IResponsePtr& resp, const IRequestPtr& req)>;
using dynamic_req_handler_t = std::function<void(const IResponsePtr& resp, const IRequestPtr& req, const std::vector<std::string>& params)>;

typedef std::pair<RequestParamsRegEx, dynamic_req_handler_t> dynamic_route_t;
typedef std::vector<dynamic_route_t> dynamic_routes_t;

typedef std::map<RequestParams, static_req_handler_t> static_routes_t;

class RequestsRouter;

using RequestsRouterPtr = std::shared_ptr<RequestsRouter>;

class RequestsRouter {
public:
    static RequestsRouterPtr Instanse()
    {
        static RequestsRouterPtr router = RequestsRouterPtr(new RequestsRouter);
        return router;
    }

    static_req_handler_t RouteReq(const std::string& target, const net::method_t& method);

    void AddStaticEndpoint(const RequestParams& params, const static_req_handler_t& fun);

    void AddDynamicEndpoint(const RequestParamsRegEx& params, const dynamic_req_handler_t& fun);

private:
    static_routes_t m_staticRoutes;
    dynamic_routes_t m_dynamicRoutes;

    RequestsRouter();
    RequestsRouter(RequestsRouter&) = delete;

    static_req_handler_t WrapDynamicRequest(const std::string& target, const dynamic_route_t& route);
};

#endif // DETAILSROUTER_H
