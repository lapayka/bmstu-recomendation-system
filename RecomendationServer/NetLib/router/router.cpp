#include "router.h"

#include <logger/LoggerFactory.h>

#include <iostream>

static_req_handler_t RequestsRouter::RouteReq(const std::string& target, const net::method_t& method)
{
    RequestParams static_params { target, method };
    static_req_handler_t creator;

    if (m_staticRoutes.contains(static_params))
        creator = m_staticRoutes[static_params];

    if (!creator) {
        for (const auto& dynRoute : m_dynamicRoutes) {
            if (std::regex_match(target, dynRoute.first.target) && method == dynRoute.first.method) {
                creator = WrapDynamicRequest(target, dynRoute);
                break;
            }
        }
    }

    if (!creator) {
        LoggerFactory::GetLogger()->LogWarning(std::string("Wrong endpoint: " + target).c_str());
    }

    return creator;
}

void RequestsRouter::AddStaticEndpoint(const RequestParams& params, const static_req_handler_t& fun) { m_staticRoutes[params] = fun; }

void RequestsRouter::AddDynamicEndpoint(const RequestParamsRegEx& params, const dynamic_req_handler_t& fun)
{
    m_dynamicRoutes.push_back(dynamic_route_t(params, fun));
}

RequestsRouter::RequestsRouter() {}

static_req_handler_t RequestsRouter::WrapDynamicRequest(const std::string& target, const dynamic_route_t& route)
{
    std::smatch match;
    std::regex_search(target, match, route.first.target);

    std::vector<std::string> params;
    for (size_t i = 1; i < match.size(); i++) {
        params.push_back(match[i].str());
    }

    return std::bind(route.second, std::placeholders::_1, std::placeholders::_2, params);
}
