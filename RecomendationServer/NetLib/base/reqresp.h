#ifndef REQRESP_H
#define REQRESP_H

#include <exceptions/server_exceptions.h>

#include <map>
#include <memory>
#include <string>
#include <vector>

// TODO change to map
typedef std::map<std::string, std::string> headers_t;

#ifdef DELETE
    #undef DELETE
#endif

namespace net {
enum method_t { GET, POST, DELETE, PUT, PATCH, UNDEFINED };

enum http_code_t {
    CODE_200 = 200, // OK
    CODE_400 = 400, // bad request
    CODE_401 = 401, // unauthorized
    CODE_403 = 403, // forbidden
    CODE_404 = 404, // not found
    CODE_503 = 503  // internal server error
};

} // namespace net

// Сюда можно добавлять структуры и парсить их из json
struct ExtraData {
    ExtraData() = default;
    // ExtraData(const std::string& json_str) {
    //     Json::Value value;
    //     Json::Reader reader;

    //     bool parse_successfull = reader.parse(json_str, value);

    //     if (!parse_successfull) {
    //         throw JsonParserException("can't parse extra data");
    //     }

    //     auth_inf.worker_id = value["worker_id"].asUInt64();
    // }
    struct AuthInf {
        size_t worker_id;
    } auth_inf;
};

// TODO разбить на несколько файлов

class IRequest;
using IRequestPtr = std::shared_ptr<IRequest>;

class IRequest {
public:
    virtual ~IRequest() = default;

    virtual std::string GetBody() const = 0;
    virtual std::string GetTarget() const = 0;
    virtual headers_t GetHeaders() const = 0;
    virtual net::method_t GetMethod() const = 0;
    virtual ExtraData GetExtraData() const = 0;

    virtual void SetBody(const std::string& body) = 0;
    virtual void SetHeaders(const headers_t& headers) = 0;
    virtual void SetTarget(const std::string& target) = 0;
    virtual void SetMethod(const net::method_t& method) = 0;
    virtual void SetExtraData(const ExtraData& extra_data) = 0;

    void copy(const IRequestPtr& another_req)
    {
        this->SetBody(another_req->GetBody());
        this->SetTarget(another_req->GetTarget());
        this->SetHeaders(another_req->GetHeaders());
        this->SetMethod(another_req->GetMethod());
        this->SetExtraData(another_req->GetExtraData());
    }
};

class IResponse;

using IResponsePtr = std::shared_ptr<IResponse>;

class IResponse {
public:
    virtual ~IResponse() = default;
    virtual std::string GetBody() const = 0;
    virtual void SetBody(const std::string& body) = 0;

    virtual headers_t GetHeaders() const = 0;
    virtual void SetHeaders(const headers_t& headers) = 0;

    virtual net::http_code_t GetStatus() const = 0;
    virtual void SetStatus(net::http_code_t status) = 0;

    void copy(const IResponsePtr& another_resp)
    {
        this->SetBody(another_resp->GetBody());
        this->SetHeaders(another_resp->GetHeaders());
        this->SetStatus(another_resp->GetStatus());
    }
};

#endif // REQRESP_H
