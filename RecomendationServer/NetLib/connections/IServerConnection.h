#pragma once

#include <memory>

class IServerConnection {
public:
    virtual void Run() = 0;

    virtual ~IServerConnection() = 0;
};

using IServerConnectionPtr = std::shared_ptr<IServerConnection>;