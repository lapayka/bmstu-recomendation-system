#pragma once

#include <common_macros.h>
#include <config/base_config.h>
#include <sessions/base_session.h>

#include <boost/asio.hpp>
#include <boost/di.hpp>
#include <memory>
#include <string>
#include <vector>

#include "IServerConnection.h"

using coroutine_session_t = std::pair<std::shared_ptr<IBaseServerSession>, std::future<void>>;

DI_NAME_DECLARE(ioContext)

class ServerConnection
    : public IServerConnection
    , public std::enable_shared_from_this<ServerConnection> {
public:
    BOOST_DI_INJECT(ServerConnection, (named = ioContext) asio::io_context& context, const std::shared_ptr<IServerSessionCreator>& creator,
        const std::shared_ptr<BaseConfig>& config);

    virtual void Run() override;

    virtual ~ServerConnection() = default;

protected:
    void Fail(const error_code& ec, const std::string& desc);

private:
    std::vector<coroutine_session_t> m_coroutineSessions;
    std::mutex m_coroutineSessionsEraseMutex;
    std::shared_ptr<IServerSessionCreator> m_sessionCreator;
    tcp::acceptor m_acceptor;
    asio::io_context& m_context;
    std::shared_ptr<BaseConfig> m_config;

    void AcceptNew();
    void ClearExpiredConnections();
};
