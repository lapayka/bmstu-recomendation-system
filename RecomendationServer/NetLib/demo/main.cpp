#include <base/BeastResponseFactory.h>
#include <config/yamlcpp_config.h>
#include <connections/server_connection.h>
#include <logger/LoggerFactory.h>
#include <router/router.h>
#include <sessions/http_session.h>

#include <boost/di.hpp>
#include <memory>

#include "dummy_server_handler.h"
#include "dummy_server_handler_creator.h"
namespace di = boost::di;

void Check(const IResponsePtr& resp, const IRequestPtr&, const std::vector<std::string>& params)
{
    if (params.size() != 1)
        return resp->SetStatus(net::CODE_503);

    resp->SetBody(params[0]);
    resp->SetStatus(net::CODE_200);
}

void Info(const IResponsePtr& resp, const IRequestPtr&)
{
    resp->SetBody("status: ok");
    resp->SetStatus(net::CODE_200);
}

void SetupRouter()
{
    RequestsRouter::Instanse()->AddStaticEndpoint({ "/info", net::GET }, Info);
    RequestsRouter::Instanse()->AddDynamicEndpoint({ std::regex("/check/([0-9\\-]+)"), net::GET }, Check);
}

int main(int argc, char* argv[])
{
    if (argc != 2) {
        return 1;
    }

    SetupRouter();

    asio::io_context ioc;

    auto injector = di::make_injector(di::bind<BaseConfig>().to<YamlCppConfig>(),
        di::bind<std::string>.named(configFileName).to(std::string(argv[1])),
        di::bind<IResponseFactory>().to<BeastResponseFactory>().in(di::singleton),
        di::bind<IServerReqHandlerCreator>.to<DummyServerReqHandlerCreator>(), di::bind<asio::io_context>.named(ioContext).to(ioc),
        di::bind<IServerSessionCreator>().to<HttpServerSessionCreator>().in(di::singleton),
        di::bind<IServerConnection>().to<ServerConnection>());

    injector.create<std::shared_ptr<IServerSessionCreator>>()->CreateSession();

    injector.create<IServerConnectionPtr>()->Run();
    return 0;
}
