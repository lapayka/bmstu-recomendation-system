#pragma once

#include <base/IResponseFactory.h>
#include <base/server_req_handler.h>

#include "dummy_server_handler.h"

class DummyServerReqHandlerCreator : public IServerReqHandlerCreator {
public:
    DummyServerReqHandlerCreator(const IResponseFactoryPtr& factory);

    virtual std::shared_ptr<IServerReqHandler> CreateHandler() const override;

private:
    IResponseFactoryPtr m_factory;
};