#pragma once
#include "IFilter.h"

class HasTunerFilter :
    public IFilter
{
public:
	virtual bool Check(const GuitarItem& item) const override { return item.hasTuner; };

	virtual ~HasTunerFilter() = default;
};

