#pragma once

#include "GuitarItem.h"
#include <set>

class TypeTreeNode;

using TypeTreeNodePtr = std::shared_ptr<TypeTreeNode>;

class TypeTreeNode 
{
public:
	static TypeTreeNodePtr Instance();

	std::set<GuitarItem::Shape> GetShapes();
	size_t GetDistance(GuitarItem::Shape left, GuitarItem::Shape right);
	std::shared_ptr<TypeTreeNode> GetNode(GuitarItem::Shape left);

	TypeTreeNode(GuitarItem::Shape shape)
		: m_shape(shape) {}
	TypeTreeNode(const std::vector <std::shared_ptr<TypeTreeNode>>& nodes, GuitarItem::Shape shape)
		: m_nodes(nodes),
		m_shape(shape) {}

	TypeTreeNode(const TypeTreeNode&) = default;
private:
	std::vector <std::shared_ptr<TypeTreeNode>> m_nodes;
	GuitarItem::Shape m_shape;

	std::vector<int> GetWay(GuitarItem::Shape shape);
};

